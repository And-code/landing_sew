<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use App\IconsGrid;
use App\ImageShowcase;
use App\Lang;
use App\Testimonial;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Throwable;
use Illuminate\Support\Arr;

/**
 * Description of HelperFunctions
 *
 * @author andrey
 */
class HelperFunctions {


    public static function getTitleAdmin($locale) {
        $titlePrefix = Cache::get('allSections_' . $locale)['otherFields']['sectionData']['adminPageTitlePrefix'];
        $titleMain = Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'];

        $title = $titlePrefix . $titleMain;
        return $title;
    }



    public static function setCache($locale) {

        $res_data = Cache::remember('allSections_' . $locale, 3600, function () use ($locale) {

            $allSections = HelperFunctions::getDataForCache($locale);

            return  $allSections;
        });

        $res_routes = Cache::remember('allRoutes_' . $locale, 3600, function () use ($locale) {

            $allRoutes = HelperFunctions::getAllRoutes($locale);

            return  $allRoutes;
        });

        if ($res_data === false ) {
            Cache::flush();
//            Cache::forget('allSections_' . $locale);
//            Cache::forget('allRoutes_' . $locale);
            return response('Data caching error...');
        } elseif ($res_routes === false) {
            Cache::flush();
//            Cache::forget('allSections_' . $locale);
//            Cache::forget('allRoutes_' . $locale);
            return response('Routes caching error...');
        }

        $res_data_json = Cache::remember('allSections_json_' . $locale, 3600, function () use ($res_data) {
            return json_encode($res_data);
        });

        $res_routes_json = Cache::remember('allRoutes_json_' . $locale, 3600, function () use ($res_routes) {
            return json_encode($res_routes);
        });

        if ($res_data_json === false ) {
            Cache::flush();
            return response('Data (json) caching error...');
        } elseif ($res_routes_json === false) {
            Cache::flush();
            return response('Routes (json) caching error...');
        }
    }


    public static function getDataForCache($locale) {
        $allSections = [];

//        $item->withoutEvents(function () use ($item){
//            $item->save();

        try {
            // проверка видимости секции
            $visibilities1 = Lang::where('lang', $locale)->first();
            $visibilities = $visibilities1->visibilities->toQuery()->get(['sectionName', 'visibility']);

            // загружаем данные для каждой видимой секции
            foreach ($visibilities as $section) {
                if ($section->visibility == true) {

                    $sectionData = null;
                    switch ($section->sectionName) {
                        case 'navbar':
//                            $sectionData = VariousData::where(['name' => 'navbar', 'lang' => $locale])->get('properties')->first()->properties;
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'navbar')->get('properties')->first()->properties;
                            break;
                        case 'header':
//                            $sectionData = VariousData::where(['name' => 'header', 'lang' => $locale])->get('properties')->first()->properties;
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'header')->get('properties')->first()->properties;
                            break;
                        case 'callToActions':
//                            $sectionData = VariousData::where(['name' => 'callToActions', 'lang' => $locale])->get('properties')->first()->properties;
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'callToActions')->get('properties')->first()->properties;
                            break;
                        case 'icons_grids':
//                            $sectionData = IconsGrid::where('lang', $locale)->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();
                            $sectionData = $visibilities1->iconsGrids->toQuery()->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'image_showcases':
//                            $sectionData = ImageShowcase::where('lang', $locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            $sectionData = $visibilities1->imageShowcases->toQuery()->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'testimonials':
//                            $sectionData = Testimonial::where('lang', $locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            $sectionData = $visibilities1->testimonials->toQuery()->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'testimonialsHeader':
//                            $sectionData = VariousData::where(['name' => 'testimonials', 'lang' => $locale])->get('properties')->first()->properties['mainHeaderValue'];
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'testimonials')->get('properties')->first()->properties['mainHeaderValue'];
                            break;
                        case 'footer':
//                            $sectionData = VariousData::where(['name' => 'footer', 'lang' => $locale])->get('properties')->first()->properties;
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'footer')->get('properties')->first()->properties;
                            break;
                        case 'otherFields':
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'otherFields')->get('properties')->first()->properties;
                            break;
                    }
                    $allSections[$section->sectionName] = [
                        'visibility' => true,
                        'sectionData' => $sectionData,
                    ];
                } else {
                    $allSections[$section->sectionName] = [
                        'visibility' => false,
                        'hiddenSectionValue' => "Раздел \"$section->sectionName\" скрыт",
                    ];
                }

            }
        }catch (Throwable $e) {
            return false;
        }




        return  $allSections;
    }

    public static function getAllRoutes($locale) {
        $routes =  Route::getRoutes();

        $arr = [];
        foreach ($routes as $route) {

            if (strpos($route->getName(), 'land.') !== false &&
                (in_array('GET', $route->methods()) || strpos($route->getName(), 'visibility.update') !== false)
            ) {
                $url = route("" . $route->getName(), ['locale' => $locale ]);

                $arr[$route->getName()] =  $url;
            }
        }

        // routes for choice of languages
        $tempArr = [];
        $url = url('/');
//        $langs = \App\Lang::get('lang')->toArray();
        $langs = \App\Lang::where('visibility', true)->get('lang')->toArray();

        foreach ($langs as $lang) {
//            $tempArr[$lang['lang']] = $url . "/". $lang['lang'];
            $tempArr[] = [
                'url' => $url . "/". $lang['lang'],
                'lang' => $lang['lang'],
            ];
        }

        $arr['langs.routes'] = $tempArr;

        return $arr;
    }


    // выбор языка для редиректа
    public static function checkLocale($locale = null, $isAdmin = false) {

        $defaultLocale = app()->getLocale();
        $langs = \App\Lang::where('visibility', true)->get();
        $langsAll = \App\Lang::all();

        if ($langs === null) {
//            return response('Нет локали');
            return null;
        }

        if ($locale === null) {
//        $langs = \App\Lang::where('visibility', true)->where('lang', $locale)->first();
            $lang = $langs->where('lang', $defaultLocale)->first();
            // если локаль не задана или если локаль по дефолту невидима, то берем первую доступную локаль
            $langToSwitch = $lang === null ? $langs->first()->lang : $lang->lang;

            return $langToSwitch;
        } else {
            $lang = null;
            if ($isAdmin) {
                $lang = $langsAll->where('lang', $locale)->first();
            } else {
                $lang = $langs->where('lang', $locale)->first();
            }

            if ($lang === null) {
                $lang = $langs->where('lang', $defaultLocale)->first();
            }
            // если локаль не задана или если локаль по дефолту невидима, то берем первую доступную локаль
            $langToSwitch = $lang === null ? $langs->first()->lang : $lang->lang;
            return $langToSwitch;
        }
    }

    // при удалении локали - удалить ее кеш
    public static function deleteCacheForLocale($locale) {
        // удаляем кешированные данные конкретной локали
        $res1 = Cache::forget('allSections_' . $locale);
        $res2 = Cache::forget('allRoutes_' . $locale);
        $res3 = Cache::forget('allSections_json_' . $locale);
        $res4 = Cache::forget('allRoutes_json_' . $locale);

    }

    // при изменении данных конкретной локали
    public static function updateCacheForLocale($locale) {

        // удаляем кешированные данные конкретной локали
        HelperFunctions::deleteCacheForLocale($locale);

        // заново создаем кеш для конкретной локали
        HelperFunctions::setCache($locale);
    }

    // кеширование маршрутов для смены локали
    public static function setRoutesForLocaleChangeCache() {

//        $langs = Lang::all();

//        $routesForLangChange = [];
//
//        foreach ($langs as $lang) {
//            $routesForLangChange[$lang->id] = route('land.langs.index', $lang->lang);
//        }



        $res_data = Cache::remember('routesForLangChange', 3600, function () {

            $langs = Lang::all();

            $routesForLangChange = [];

            foreach ($langs as $lang) {
                $routesForLangChange[$lang->id] = route('land.indexContentLangsEdit', ['locale' => $lang->lang]);
            }


//            return  json_encode($routesForLangChange);
            return  $routesForLangChange;
        });



        if ($res_data === false ) {
            Cache::forget('routesForLangChange');

            return response('RoutesForLangChange caching error...');
        }

    }

    // обновляем кеш маршрутов для смены локали
    public static function updateRoutesForLocaleChangeCache() {
//        Cache::forget('routesForLangChange');
        Cache::flush();
        HelperFunctions::setRoutesForLocaleChangeCache();
    }


    public static function getDataForTranslate($locale) {
        $allSections = [];


        try {
            // проверка видимости секции
            $visibilities1 = Lang::where('lang', $locale)->first();
            $visibilities = $visibilities1->visibilities->toQuery()->get(['sectionName', 'visibility']);

            // загружаем данные для каждой секции независимо от видимости
            foreach ($visibilities as $section) {
                    $sectionData = null;
                    switch ($section->sectionName) {
                        case 'navbar':
                            // OK
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'navbar')->get('properties')->first()->properties;
                            break;
                        case 'header':
                            // OK
                            $props = $visibilities1->variousDatas->toQuery()->where('name', 'header')->get('properties')->first()->properties;
                            $sectionData = Arr::except($props, ['imgUrl']);
//                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'header')->get('properties')->first()->properties;
                            break;
                        case 'callToActions':
                            // OK
                            $props = $visibilities1->variousDatas->toQuery()->where('name', 'callToActions')->get('properties')->first()->properties;
                            $sectionData = Arr::except($props, ['imgUrl']);
                            break;
                        case 'icons_grids':
                            //OK
                            $sectionData = $visibilities1->iconsGrids->toQuery()->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'image_showcases':
                            $sectionData = $visibilities1->imageShowcases->toQuery()->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'testimonials':
                            $sectionData = $visibilities1->testimonials->toQuery()->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();
                            break;
                        case 'testimonialsHeader':
                            // OK
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'testimonials')->get('properties')->first()->properties['mainHeaderValue'];
                            break;
                        case 'footer':
                            // OK
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'footer')->get('properties')->first()->properties;
                            break;
                        case 'otherFields':
                            // OK
                            $sectionData = $visibilities1->variousDatas->toQuery()->where('name', 'otherFields')->get('properties')->first()->properties;
                            break;
                    }
                    $allSections[$section->sectionName] = $sectionData;


            }
        }catch (Throwable $e) {
            return false;
        }




        return  $allSections;
    }

}
