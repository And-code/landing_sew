<?php

namespace App;

use App\Helpers\HelperFunctions;
use App\Traits\LocaleForModel;
use Illuminate\Database\Eloquent\Model;

class Visibility extends Model
{

    use LocaleForModel;

    //
    protected $fillable = ['sectionName', 'visibility', 'lang_id'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'visibility' => 'boolean',
    ];


    public function lang () {
        return $this->belongsTo('App\Lang');
    }

}
