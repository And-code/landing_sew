<?php

namespace App;

use App\Helpers\HelperFunctions;
use App\Traits\LocaleForModel;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use LocaleForModel;

    //
    protected $fillable = ['imgUrl', 'headerValue', 'pValue', 'lang_id'];


    public function lang () {
        return $this->belongsTo('App\Lang');
    }
}
