<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Cache;

class ValidInterfaceKeys implements Rule
{
    private $keysDiff = [];
    private $messagesJsonValidation = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->messagesJsonValidation = Cache::get('allSections_' . app()->getLocale())
                ['otherFields']['sectionData']['interface']['langInterface'];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        $data = $request->interfaceTranslationNew;
        $data = null;
        if (!is_array($value)) {
            $data = json_decode($value, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                // JSON is not valid
                return false;
            }
        } else {
            $data = $value;
        }



//        $data = $value;


        $interface = Cache::get('allSections_' . app()->getLocale())['otherFields']['sectionData'];

        $data_keys = $this->recursiveGetKeysArr($data);
        $interface_keys = $this->recursiveGetKeysArr($interface);

        $result=array_udiff_assoc($data_keys, $interface_keys, function ($a,$b) {
            if ($a===$b)
            {
                return 0;
            }
            return ($a>$b)?1:-1;
        });

        $diffKeys = $this->recursiveGetKeysDiff($data_keys, $interface_keys);

//        $this->keysDiff = $result;
        $this->keysDiff = $diffKeys;

        $size = sizeof($result);

            return sizeof($result) === 0 ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
//        return 'The validation error message.';
//        $temp = json_encode( $this->keysDiff, JSON_PRETTY_PRINT);
//        return 'The validation error message.' . json_encode( $this->keysDiff, JSON_PRETTY_PRINT);
//        return 'The validation error message. ' . json_encode( $this->keysDiff);

        $message = Cache::get('allSections_' . app()->getLocale())
                        ['otherFields']['sectionData']['interface']['validationMessages']['jsonError'];
        return $message . ' '. json_encode( $this->keysDiff, JSON_UNESCAPED_UNICODE);
    }

    public function recursiveGetKeysDiff($data_keys, $interface_keys) {
        $keys = [];
        foreach ($interface_keys as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists($key, $data_keys)) {
                    if (is_array($data_keys[$key])) {
//                        $keys[$key] = $this->recursiveGetKeysDiff($value, $interface_keys[$key]);
                        $localRes = $this->recursiveGetKeysDiff($data_keys[$key], $value);
                        if (!empty($localRes)) {
                            $keys[$key] = $localRes;
                        }
                    } else {
                        // было - стало
//                        $keys[$key] = $data_keys[$key] . ' (become not Array). Must be: ' . json_encode($value);
                        $keys[$key] = $data_keys[$key] . $this->messagesJsonValidation['becomeNotArray'] . json_encode($value);
                    }
                } else {
                    // если в новом JSON не существует уже такого ключа
//                    $keys[$key] = "Key '$key' must be in JSON";
                    $keys[$key] = $this->messagesJsonValidation['mustPresentInJson'] . "(" .$key . ")";
                }

            } elseif (array_key_exists($key, $data_keys)) {
                // если в новом JSON значение этого ключа вдруг стало массивом
                if (is_array($data_keys[$key])) {
//                    $keys[$key] = $value . ' (Must be a String! Become Array in new JSON?)';
                    $keys[$key] = $value . $this->messagesJsonValidation['mustBeString'];
                } else {
                    if ($value !== $data_keys[$key]) {
//                        $keys[$value] = "Key '$data_keys[$key]' must be '$value'";
                        $keys[$value] = $this->messagesJsonValidation['valueOfKeyMustBe'] . $data_keys[$key] . "=". $value;
                    } else {
//                        $keys[$value] = $interface_keys[$key];
                    }
                }
            } else {
                // если в новом JSON не существует уже такого ключа
                $qqq = is_string($value) ? $value : $key;
//                $keys[$qqq] = "Key '$qqq' must be in JSON";
                $keys[$qqq] = $this->messagesJsonValidation['mustPresentInJson'] . "(" .$qqq . ")";
            }
        }
        return $keys;
    }


    // вспомогательная функция
    public function recursiveGetKeysArr($arrInput) {
        $keys = [];
        foreach ($arrInput as $key => $value) {
            if (is_array($value)) {
                $keys[] = [
                    $key => $this->recursiveGetKeysArr($value)
                ];
            } else {
                $keys[] = $key;
            }
        }
//        foreach ($arrInput as $key => $value) {
//            if (is_array($value)) {
//                $keys[$key] = $this->recursiveGetKeysArr($value);
//            } else {
//                $keys[$key] = $key;
//            }
//        }
        return $keys;
    }
}
