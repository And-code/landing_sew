<?php

namespace App;

use App\Helpers\HelperFunctions;
use App\Traits\LocaleForModel;
use Illuminate\Database\Eloquent\Model;

class IconsGrid extends Model
{

    use LocaleForModel;

    //
    protected $fillable = ['iconName', 'headerValue', 'pValue', 'lang_id'];


    public function lang () {
        return $this->belongsTo('App\Lang');
    }

}
