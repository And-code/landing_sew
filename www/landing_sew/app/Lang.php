<?php

namespace App;

use App\Helpers\HelperFunctions;
use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    protected $fillable = ['lang', 'visibility'];

    protected $casts = [
        'visibility' => 'boolean',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function ($lang) {
            //
//            HelperFunctions::updateCacheForLocale($lang->lang);

            // обновляем кеш маршрутов для смены локали
            HelperFunctions::updateRoutesForLocaleChangeCache();
        });

        static::deleted(function ($lang) {
            //
//            HelperFunctions::deleteCacheForLocale($lang->lang);
            // обновляем кеш маршрутов для смены локали
            HelperFunctions::updateRoutesForLocaleChangeCache();
        });

    }


    /**
     * Get the iconsGrids for the lang.
     */
    public function iconsGrids()
    {
        return $this->hasMany('App\IconsGrid');
    }

    /**
     * Get the imageShowcases for the lang.
     */
    public function imageShowcases()
    {
        return $this->hasMany('App\ImageShowcase');
    }

    /**
     * Get the testimonials for the lang.
     */
    public function testimonials()
    {
        return $this->hasMany('App\Testimonial');
    }

    /**
     * Get the variousDatas for the lang.
     */
    public function variousDatas()
    {
        return $this->hasMany('App\VariousData');
    }

    /**
     * Get the visibilities for the lang.
     */
    public function visibilities()
    {
        return $this->hasMany('App\Visibility');
    }


}
