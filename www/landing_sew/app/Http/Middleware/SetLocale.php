<?php

namespace App\Http\Middleware;

use App\Helpers\HelperFunctions;
use App\Lang;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;


class SetLocale extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale_request = $request->segment(1);
        $isAdmin = $request->segment(2) === 'admin';

        $langToSwitch = HelperFunctions::checkLocale($locale_request, $isAdmin);

        if ($langToSwitch === null) {
            return response('Нет локали');
        } elseif ($langToSwitch === $locale_request) {
            app()->setLocale($langToSwitch);
        } else {
            return redirect($langToSwitch);
        }



        return $next($request);
    }
}
