<?php

namespace App\Http\Middleware;

use App\Helpers\HelperFunctions;
use App\IconsGrid;
use App\ImageShowcase;
use App\Testimonial;
use App\VariousData;
use App\Visibility;
use Closure;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Cache;

class SetCache extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $locale = app()->getLocale();

//        Cache::flush();

        HelperFunctions::setCache($locale);

        return $next($request);
    }
}
