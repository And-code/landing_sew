<?php

namespace App\Http\Middleware;

use Closure;

class CheckVisibilityParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'visibility' => 'required|boolean',
            'sectionName' => 'required',
        ]);

        return $next($request);
    }
}
