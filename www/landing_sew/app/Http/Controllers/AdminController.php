<?php

namespace App\Http\Controllers;

use App\Helpers\HelperFunctions;
use App\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Cache;

class AdminController extends Controller
{

    // GET - main admin page
    public function show($locale) {


        $cache = Cache::get('allSections_' . $locale);

        $title = HelperFunctions::getTitleAdmin($locale);

//        $interface = json_encode(Cache::get('allSections_' . $locale)['otherFields']['sectionData']);
        $interface = Cache::get('allSections_' . $locale)['otherFields']['sectionData'];

        return view('admin_main', [
                'title' => $title,
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
                'interface' => $interface,
            ]
        );
    }

    // GET - admin footer edit page
    public function showFooterEdit($locale) {
//        return view('layout_admin_footer_ext');

        $cache = Cache::get('allSections_' . $locale);


//        $title = $cache['otherFields']['sectionData']['adminPageTitlePrefix'];
        $title = HelperFunctions::getTitleAdmin($locale);

        $interface = Cache::get('allSections_' . $locale)['otherFields']['sectionData'];

        return view('admin_footer_edit', [
                'title' => $title,
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'interface' => $interface,
            ]
        );
    }

    public function showContentLangsEdit($locale) {

        // кешируем маршруты для смены локали
        HelperFunctions::setRoutesForLocaleChangeCache();
//        HelperFunctions::updateRoutesForLocaleChangeCache();

        $title = HelperFunctions::getTitleAdmin($locale);
        $sections = Cache::get('allSections_json_' . $locale);
        $routes = Cache::get('allRoutes_json_' . $locale);
        $langs = Lang::get(['id', 'lang', 'visibility'])->toJson();

        $routesForLangChange = json_encode(Cache::get('routesForLangChange'));

        $interface = Cache::get('allSections_' . $locale)['otherFields']['sectionData'];

        return view('admin_content_langs_edit', [
                'title' => $title,
                'sections' => $sections,
                'routes' => $routes,
                'langs' => $langs,
                'locale' => $locale,
                'routes_for_lang_change' => $routesForLangChange,
                'interface' => $interface,
            ]
        );
    }

    public function showInterfaceLangsEdit($locale) {

        // кешируем маршруты для сметы локали
//        HelperFunctions::setRoutesForLocaleChangeCache();
//        HelperFunctions::updateRoutesForLocaleChangeCache();

        $title = HelperFunctions::getTitleAdmin($locale);
        $sections = Cache::get('allSections_json_' . $locale);
        $routes = Cache::get('allRoutes_json_' . $locale);
//        $langs = Lang::get(['id', 'lang', 'visibility'])->toJson();

//        $routesForLangChange = json_encode(Cache::get('routesForLangChange'));

        $interface = Cache::get('allSections_' . $locale)['otherFields']['sectionData'];

        return view('admin_interface_langs_edit', [
                'title' => $title,
                'sections' => $sections,
                'routes' => $routes,
                'locale' => $locale,
                'interface' => $interface,
            ]
        );
    }

}
