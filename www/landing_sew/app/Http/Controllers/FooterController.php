<?php

namespace App\Http\Controllers;

use App\VariousData;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class FooterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        $footer_db = VariousData::locale($locale)->where('name', 'footer')->get('properties')->first()->properties;

        return response()->json([
            'footer' => $footer_db,
        ]);
    }

    // получение списка иконок из css
    public function iconsSocial() {
        $path = 'vendor/fontawesome-free/css/all.css';
        $file = fopen($path, 'r');

        if ($file === false) {
            return response()->json([
                'iconsSocial'=> 'No data',
            ]);
        }

        $arr = [];
        while (!feof($file)) {
            $str = fgets($file);
            if (strpos($str, '.fa-') === 0 &&
                strpos($str, ':before') !== false) {
                $arr[] = (preg_split("/[.:]+/", $str))[1];
            }
        }
        fclose($file);

        $uniqe_arr = array_unique($arr);

        // преобразование массива
        foreach ($uniqe_arr as &$row) {
            $row = [
                'value' => $row,
                'text' => $row,
            ];
        }

        return response()->json([
            'iconsSocial' => $uniqe_arr,
        ]);
}


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(Request $request, $locale)
    {
        // обновляем только footer
        if ($request->has('footer')) {

            $position = VariousData::locale($locale)->where('name', 'footer')->get()->first();
            $position->properties = $request->footer;
            $position->save();

            $response_object = [
                'status' => 'success',
//                'message' => 'Footer updated!',
                'message' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['footer']['messageUpdate'],
            ];
            $response_object['footer'] = $position->properties;


            return response()->json($response_object, 200);

        } else {
            return response('', 500);
        }
    }

}
