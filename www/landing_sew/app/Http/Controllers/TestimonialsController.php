<?php

namespace App\Http\Controllers;

use App\ImageShowcase;
use App\Lang;
use App\Testimonial;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        // проверка видимости секции
        $visibility_db_all = Visibility::locale($locale)->whereIn('sectionName', ['testimonials', 'testimonialsHeader'])->get(['sectionName', 'visibility']);

        $visibility_db = $visibility_db_all->where('sectionName', 'testimonials')->first()->visibility;

        if ($visibility_db == true) {
            $testimonials_db = Testimonial::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $mainHeaderValue_db = VariousData::locale($locale)->where('name', 'testimonials')->get('properties')->first()->properties['mainHeaderValue'];

            $mainHeaderVisibility_db = $visibility_db_all->where('sectionName', 'testimonialsHeader')->first()->visibility;

            return response()->json([
                'visibility' => true,
                'testimonials' => $testimonials_db,
                'mainHeaderVisibility' => $mainHeaderVisibility_db,
                'mainHeaderValue' => $mainHeaderValue_db,
            ]);

        } else {
            return response()->json([
                'visibility' => false,
//                'hiddenSectionValue' => 'Раздел "Icons Grids" скрыт',
                'hiddenSectionValue' => Cache::get('allSections_' . $locale)
                                        ['otherFields']['sectionData']['interface']['testimonial']['hiddenSectionValue'],
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $locale)
    {
        if ($request->has(['file', 'headerValue', 'pValue']) && $request->file('file')->isValid()) {

            $data = $request->only(['headerValue', 'pValue']);

            $data['imgUrl'] = '';
            $data['lang_id'] = Lang::where('lang', $locale)->first()->id;

            $testimonial = Testimonial::create($data);

            // добавляем файл --- начало ----
            $nameOriginal = $request->file->getClientOriginalName();
            $newName = 'testimonials-' . $testimonial->id .'-' . $nameOriginal;

            // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
            // сейчас настроено на прямое сохранение в public, а не storage.
            $path_save = $request->file->storeAs('/uploads/images', $newName);
            $path = '/uploads/images/'. $newName;

            $testimonial->imgUrl = $path;
            $testimonial->save();
            // добавляем файл --- конец ----

            // получаем данные про все записи из базы данных
            $testimonials_db = Testimonial::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'Testimonial added!'
                'message' => Cache::get('allSections_' . $locale)
                                        ['otherFields']['sectionData']['interface']['testimonial']['messageAdd'],
            ];

            $response_object['testimonials'] = $testimonials_db;

            return response()->json($response_object, 201);
        } else {
            return response('', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        if ($id == 0 && $request->has('mainHeaderValue')) {
            $position = VariousData::locale($locale)->where('name', 'testimonials')->get()->first();
            $data = $request->mainHeaderValue;
            $position->properties = ['mainHeaderValue' => $request->mainHeaderValue];
            $position->save();

            $response_object = [
                'status' => 'success',
//                'message' => 'Header for testimonials updated!',
                'message' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['testimonial']['messageUpdateHeader'],
            ];
            $response_object['mainHeaderValue'] = $position->properties['mainHeaderValue']; // $position->mainHeaderValue;

            return response()->json($response_object, 200);
        } else {
            $testimonial = Testimonial::locale($locale)->where('id', $id)->first();
            if ($testimonial === null) {
                return response('', 500);
            }

            // обновляем все поля, кроме заголовка

            // Обновляем картинку
            if ($request->hasFile('file') && $request->file('file')->isValid()) {

                $nameOriginal = $request->file->getClientOriginalName();
                $newName = 'testimonials-' . $id .'-' . $nameOriginal;

                // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
                // сейчас настроено на прямое сохранение в public, а не storage.
                $path_save = $request->file->storeAs('/uploads/images', $newName);
                $path = '/uploads/images/'. $newName;

                // удаляем предыдущую картинку если она не default
                if ($path != null &&
                    !in_array($request->imgUrl, ['/img/testimonials-1.jpg', '/img/testimonials-2.jpg', '/img/testimonials-3.jpg']) &&
                    $request->imgUrl != $path
                ) {
                    $path_to_del = public_path() . $request->imgUrl;
                    $res_bool = File::locale($locale)->delete($path_to_del);
                }

                $testimonial->imgUrl = $path;
                $testimonial->save();
            }

            if ($request->has(['headerValue', 'pValue'])) {

                $data = $request->only(['headerValue', 'pValue']);

                $testimonial->headerValue = $data['headerValue'];
                $testimonial->pValue = $data['pValue'];
                $testimonial->save();

                // получаем данные про все записи из базы данных
                $testimonials_db = Testimonial::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

                $response_object = [
                    'status' => 'success',
//                    'message' => 'Testimonials updated!'
                    'message' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['testimonial']['messageUpdate'],
                ];
                $response_object['testimonials'] = $testimonials_db;

                return response()->json($response_object, 200);
            } else {
                return response('', 500);
            }
        }








            // обновляем все поля, кроме заголовка
//            $index = array_search($id, array_column($this->testimonials, 'id'));
//            if ($index !== false) {
//                // Обновляем картинку
//                if ($request->hasFile('file') && $request->file('file')->isValid()) {
//
//                    $nameOriginal = $request->file->getClientOriginalName();
//                    $newName = 'testimonials-' . $id .'-' . $nameOriginal;
//
//                    // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
//                    // сейчас настроено на прямое сохранение в public, а не storage.
//                    $path_save = $request->file->storeAs('/uploads/images', $newName);
//                    $path = '/uploads/images/'. $newName;
//
//                    // удаляем предыдущую картинку
//                    $path_to_del = public_path() . $this->testimonials[$index]['imgUrl'];
//                    $res_bool = File::delete($path_to_del);
//
//                    $this->testimonials[$index]['imgUrl'] = $path;
//                }
//
//                // обновляем остальные поля
//                if ($request->has(['headerValue', 'pValue'])) {
//
//                    $data = $request->only(['headerValue', 'pValue']);
//
//                    $this->testimonials[$index]['headerValue'] = $data['headerValue'];
//                    $this->testimonials[$index]['pValue'] = $data['pValue'];
//
//                    $response_object = [
//                        'status' => 'success',
//                        'message' => 'Testimonials updated!'];
//                    $response_object['testimonials'] = $this->testimonials;
//
//                    return response()->json($response_object, 200)->cookie('testimonials', json_encode($this->testimonials), 30);
//
//                } else {
//                    return response('', 500);
//                }
//            } else {
//                return response('', 500);
//            }
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
//        $deleted_count = Testimonial::destroy($id);
        $deleted_count1 = Testimonial::locale($locale)->where('id', $id)->first();
        $deleted_count = $deleted_count1->delete();

        if ($deleted_count > 0) {

            // получаем данные про все записи из базы данных
            $testimonials_db = Testimonial::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'deleted',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['testimonial']['messageDelete'],
            ];
            $response_object['testimonials'] = $testimonials_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }

    }
}
