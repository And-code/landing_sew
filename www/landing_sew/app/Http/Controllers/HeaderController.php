<?php

namespace App\Http\Controllers;

use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;

class HeaderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        // проверка видимости секции
        $visibility_db = Visibility::locale($locale)->where('sectionName', 'header')->get('visibility')->first()->visibility;

        if ($visibility_db == true) {
            $header_db = VariousData::locale($locale)->where('name', 'header')->get('properties')->first()->properties;

            return response()->json([
                'visibility' => true,
                'header' => $header_db,
            ]);

        } else {
            return response()->json([
                'visibility' => false,
//                'hiddenSectionValue' => 'Раздел "заголовок" скрыт',
                'hiddenSectionValue' => Cache::get('allSections_' . $locale)
                        ['otherFields']['sectionData']['interface']['header']['hiddenSectionValue'],
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale)
    {

        $path = null;

        $position = VariousData::locale($locale)->where('name', 'header')->get()->first();

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            // Обновляем картинку
            $nameOriginal = $request->file->getClientOriginalName();
            $newName = 'bg-masthead-' . $nameOriginal;

            // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
            // сейчас настроено на прямое сохранение в public, а не storage.
            $path_save = $request->file->storeAs('/uploads/images', $newName);
            $path = '/uploads/images/' . $newName;

        }

        if ($request->has(['imgUrl', 'headerValue', 'placeholderValue', 'buttonValue'])) {

            // обновляем остальные поля
            $data = $request->only(['imgUrl', 'headerValue', 'placeholderValue', 'buttonValue']);

            if ($path != null) {
                $data['imgUrl'] = $path;
            }

            $position = VariousData::locale($locale)->where('name', 'header')->get()->first();
            $position->properties = $data;
            $position->save();

            // удаляем предыдущую картинку если она не default
            if ($path != null &&
                $request->imgUrl != null &&
                $request->imgUrl != '/img/bg-masthead.jpg' &&
                $request->imgUrl != $path
            ) {
                $path_to_del = public_path() . $request->imgUrl;
                $res_bool = File::delete($path_to_del);
            }

            $response_object = [
                'status' => 'success',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['header']['messageUpdate'],
                ];
//                'message' => 'Header updated!'];
            $response_object['header'] = $position->properties;

            return response()->json($response_object, 200);

        } else {
            return response('', 500);
        }


    }

}
