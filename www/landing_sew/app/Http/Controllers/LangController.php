<?php

namespace App\Http\Controllers;

use App\IconsGrid;
use App\Lang;
use App\VariousData;
use App\ImageShowcase;
use App\Testimonial;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LangController extends Controller
{

    protected $lang = [];



    public function __construct()
    {

        $id = Lang::where('lang', app()->getLocale())->first()->id;
        $res1 = VariousData::where(['name' => 'otherFields', 'lang_id' => $id])->get('properties')->first();
        $res2 = $res1->properties;
        $this->lang = $res2['interface']['lang'];

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $locale)
    {
        $messages = [
            'required' => 'The :attribute field is required.',
            'alpha' => 'The :attribute field must be alphabetic.',
        ];

$data2 = $request->all();

        $validator = Validator::make($request->all(), [
            'langTo' => 'bail|required|min:2|max:2|alpha',
            'langFrom' => 'required',
//        ])->validateWithBag('langErrors');
        ], $messages)->validate();
//
//        $validator = Validator::make($request->all(), [
//            'langTo' => 'required|alpha',
//            'langFrom' => 'required',
//        ], $messages);
//
//        if ($validator->fails()) {
//
//        }


        // дублирование существующего языка
        if ($request->filled(['langFrom', 'langTo'])) {

            $data = $request->only(['langFrom', 'langTo']);

            DB::transaction(function () use ($data, $locale){

                $res = Lang::where(['lang' => $data['langTo']])->get()->first();

//            проверяем есть ли такой язык
                if (!empty($res)) {
//                    return response('', 500);

//                    $message = Cache::get('allSections_' . $locale)
//                                    ['otherFields']['sectionData']['interface']['lang']['messageErrorDublicate'];

                    $message = $this->lang['messageErrorDublicate'];


                    throw new \Exception($message);
//                    throw new \Exception("Проверка ошибки - язык уже существует!");
                }

                // создаем новый "язык"
//                $new_lang = Lang::create(['lang' => $data['langTo'], 'visibility' => true]);
                $new_lang = Lang::withoutEvents(function () use ($data) {
                    return Lang::create(['lang' => $data['langTo'], 'visibility' => true]);
                });


                $new_lang->save();
                $lang_id = $new_lang->id;

                $routesForLangChange = Cache::get('routesForLangChange');

                $variousData = VariousData::where(['lang_id' => $data['langFrom']])->get();
                $replicatedVariousData = $this->replicateLang($variousData, $lang_id);

                $iconsGrid = IconsGrid::where(['lang_id' => $data['langFrom']])->get();
                $replicatedIconsGrid = $this->replicateLang($iconsGrid, $lang_id);

                $imageShowcase = ImageShowcase::where(['lang_id' => $data['langFrom']])->get();
                $replicatedImageShowcase = $this->replicateLang($imageShowcase, $lang_id);

                $testimonial = Testimonial::where(['lang_id' => $data['langFrom']])->get();
                $replicatedTestimonial = $this->replicateLang($testimonial, $lang_id);

                $visibility = Visibility::where(['lang_id' => $data['langFrom']])->get();
                $replicatedVisibility = $this->replicateLang($visibility, $lang_id);

            });

//            $cache = Cache::get('allSections_' . $locale);

            $response_object = [
                'status' => 'success',
//                'message' => 'Lang ' . $data['langTo'] . ' added!'
//                'message' => Cache::get('allSections_' . $locale)
//                                    ['otherFields']['sectionData']['interface']['lang']['messageAdd'] . "(". $data['langTo'] . ").",

                'message' => $this->lang['messageAdd'] . "(". $data['langTo'] . ").",
            ];

            return response()->json($response_object, 201);
        } else {
            return response('', 500);
        }


    }

    protected function replicateLang($dbData, $lang_id) {

        $replicate = collect([]);
        foreach ($dbData as $item) {
            $replicate->push($item->replicate()->fill([
                'lang_id' => $lang_id,
            ]));
        }

        $replicate->each(function ($item, $key) use ($lang_id) {
            $item->lang_id = $lang_id;
//            $item->save();
            $item->withoutEvents(function () use ($item){
                $item->save();
            });
        });

        return $replicate;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        if ($request->has(['visibility'])) {

            $data = $request->only(['visibility']);

//            $langDb = Lang::where('id', $id)->get()->first();
            $langDb = Lang::findOrFail($id);
//            $langDb->update($data);
            $langDb->withoutEvents(function () use ($langDb, $data){
                $langDb->update($data);
            });

            $res2 = Cache::forget('allRoutes_' . $locale);
            $res4 = Cache::forget('allRoutes_json_' . $locale);

            $lang = $langDb->lang;
            // получаем данные про все записи из базы данных
//            $icons_grids_db = IconsGrid::get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'Lang '. $lang .' updated'
//                'message' => Cache::get('allSections_' . $locale)
//                    ['otherFields']['sectionData']['interface']['lang']['messageUpdate'] . "(". $data['langTo'] . ").",
                'message' => $this->lang['messageUpdate'] . "(". $lang . ").",
            ];

//            $response_object['iconGrids'] = $icons_grids_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $langDb = Lang::find($id);
        $lang = $langDb->lang;

        if (Lang::all()->count() == 1) {
            $response_object = [
                'status' => 'fail',
//                'message' => 'Lang '. $lang .' last and cannot be deleted',
//                'message' => Cache::get('allSections_' . $locale)
//                    ['otherFields']['sectionData']['interface']['lang']['messageErrorDeleteLastLang'] . "(". $lang . ").",
                'message' => $this->lang['messageErrorDeleteLastLang'] . "(". $lang . ").",
            ];

            return response()->json($response_object, 200);
        }

        $deleted_count = $langDb->delete();

        if ($deleted_count === true) {
//
//            // получаем данные про все записи из базы данных
//            $icons_grids_db = IconsGrid::get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'Lang '. $lang .' deleted',
//                'message' => Cache::get('allSections_' . $locale)
//                    ['otherFields']['sectionData']['interface']['lang']['messageDelete'] . "(". $lang . ").",
                'message' => $this->lang['messageDelete'] . "(". $lang . ").",
                ];
//            $response_object['iconGrids'] = $icons_grids_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }
    }
}
