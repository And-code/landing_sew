<?php

namespace App\Http\Controllers;

use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class VisibilityController extends Controller
{
    //

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $locale)
    {

            // обновляем остальные поля
            $data = $request->only(['visibility', 'sectionName']);

//            $visibility_entity = Visibility::locale($locale)->where('sectionName', $data['sectionName'])->update(['visibility' => $data['visibility']]);
            $visibility_entity1 = Visibility::locale($locale)->where('sectionName', $data['sectionName'])->first();
            $visibility_entity = $visibility_entity1->update(['visibility' => $data['visibility']]);

            $response_object = [
                'status' => 'success',
                'message' => 'Visibility updated!'];

            return response()->json($response_object, 200);
    }
}
