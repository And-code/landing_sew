<?php

namespace App\Http\Controllers;

use App\Helpers\HelperFunctions;
use Cache;
use Illuminate\Http\Request;

class MainPageController extends Controller
{

    /** Redirect to page with the standard predefined locale (ru)
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToLocation () {
        // доделать! переводить на доступную локаль!
        $locale = null;
        $langToSwitch = HelperFunctions::checkLocale();

//        $locale = app()->getLocale();
//
////        $langs = \App\Lang::where('visibility', true)->where('lang', $locale)->first();
//        $langs = \App\Lang::where('visibility', true)->get();
//        $lang = $langs->where('lang', $locale)->first();
//        $langToSwitch = $lang === null ? $langs->first()->lang : $lang;

//        return redirect(app()->getLocale());
        return redirect($langToSwitch);
    }


    public function show(Request $request, $locale) {


        return view('main', [
            'title' => Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'],
            'sections' => Cache::get('allSections_json_' . $locale),
            'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
            ]
        );

    }


    // ---- Footer ---begin ---

    public function about(Request $request, $locale) {
        return view('info', [
            'pagetype' => 'about',
                'title' => Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'],
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
            ]
        );
    }

    public function contact(Request $request, $locale) {
        return view('info', [
            'pagetype' => 'contact',
                'title' => Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'],
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
            ]
        );
    }

    public function termsOfUse(Request $request, $locale) {
        return view('info', [
            'pagetype' => 'termsOfUse',
                'title' => Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'],
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
            ]
        );
    }


    public function privacyPolicy(Request $request, $locale) {
        return view('info', [
            'pagetype' => 'privacyPolicy',
                'title' => Cache::get('allSections_' . $locale)['navbar']['sectionData']['siteName'],
                'sections' => Cache::get('allSections_json_' . $locale),
                'routes' => Cache::get('allRoutes_json_' . $locale),
                'locale' => $locale,
            ]
        );
    }

    // ---- Footer ---end ---

}
