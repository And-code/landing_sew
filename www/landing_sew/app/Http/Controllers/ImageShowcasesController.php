<?php

namespace App\Http\Controllers;

use App\IconsGrid;
use App\ImageShowcase;
use App\Lang;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageShowcasesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        // проверка видимости секции
        $visibility_db = Visibility::locale($locale)->where('sectionName', 'image_showcases')->get('visibility')->first()->visibility;

        if ($visibility_db == true) {
            $image_showcases_db = ImageShowcase::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            return response()->json([
                'visibility' => true,
                'imageShowcases' => $image_showcases_db,
            ]);

        } else {
            return response()->json([
                'visibility' => false,
//                'hiddenSectionValue' => 'Раздел "imageShowcases" скрыт',
                'hiddenSectionValue' => Cache::get('allSections_' . $locale)
                                        ['otherFields']['sectionData']['interface']['imageShowcase']['hiddenSectionValue'],
            ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $locale)
    {
        if ($request->has(['file', 'headerValue', 'pValue']) && $request->file('file')->isValid()) {

            $data = $request->only(['headerValue', 'pValue']);

            $data['imgUrl'] = '';
            $data['lang_id'] = Lang::where('lang', $locale)->first()->id;

            $image_showcase = ImageShowcase::create($data);

            // добавляем файл --- начало ----
            $nameOriginal = $request->file->getClientOriginalName();
            $newName = 'bg-showcase-' . $image_showcase->id .'-' . $nameOriginal;

            // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
            // сейчас настроено на прямое сохранение в public, а не storage.
            $path_save = $request->file->storeAs('/uploads/images', $newName);
            $path = '/uploads/images/'. $newName;

            $image_showcase->imgUrl = $path;
            $image_showcase->save();
            // добавляем файл --- конец ----

            // получаем данные про все записи из базы данных
            $image_showcase_db = ImageShowcase::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['imageShowcase']['messageAdd']];
//                'message' => 'Image Showcase added!'];

            $response_object['imageShowcases'] = $image_showcase_db;

            return response()->json($response_object, 201);
        } else {
            return response('', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $image_showcase = ImageShowcase::locale($locale)->where('id', $id)->first();
        if ($image_showcase === null) {
            return response('', 500);
        }

        // Обновляем картинку
        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $nameOriginal = $request->file->getClientOriginalName();
            $newName = 'bg-showcase-' . $id .'-' . $nameOriginal;

            // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
            // сейчас настроено на прямое сохранение в public, а не storage.
            $path_save = $request->file->storeAs('/uploads/images', $newName);
            $path = '/uploads/images/'. $newName;

            // удаляем предыдущую картинку если она не default
            if ($path != null &&
                !in_array($request->imgUrl, ['/img/bg-showcase-1.jpg', '/img/bg-showcase-2.jpg', '/img/bg-showcase-3.jpg']) &&
                $request->imgUrl != $path
            ) {
                $path_to_del = public_path() . $request->imgUrl;
                $res_bool = File::delete($path_to_del);
            }

            $image_showcase->imgUrl = $path;
            $image_showcase->save();
        }


        if ($request->has(['headerValue', 'pValue'])) {

            $data = $request->only(['headerValue', 'pValue']);

            $image_showcase->headerValue = $data['headerValue'];
            $image_showcase->pValue = $data['pValue'];
            $image_showcase->save();

            // получаем данные про все записи из базы данных
            $image_showcase_db = ImageShowcase::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['imageShowcase']['messageUpdate']];
//                'message' => 'Image Showcase updated!'];

            $response_object['imageShowcases'] = $image_showcase_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
//        $deleted_count = ImageShowcase::destroy($id);
        $deleted_count1 = ImageShowcase::locale($locale)->where('id', $id)->first();
        $deleted_count = $deleted_count1->delete();

        if ($deleted_count > 0) {

            // получаем данные про все записи из базы данных
            $image_showcase_db = ImageShowcase::locale($locale)->get(['id', 'imgUrl', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['imageShowcase']['messageDelete']];
//                'message' => 'deleted'];
//            $response_object['imageShowcases'] = $this->imageShowcasesData;
            $response_object['imageShowcases'] = $image_showcase_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }

    }
}
