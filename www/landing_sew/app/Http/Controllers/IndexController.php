<?php

namespace App\Http\Controllers;

use App\IconsGrid;
use App\ImageShowcase;
use App\Lang;
use App\Testimonial;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        $allSections = Cache::get('allSections_' . $locale);
        $allRoutes = Cache::get('allRoutes_' . $locale);
        $allLangs = Lang::get(['id', 'lang', 'visibility'])->toArray();
        $routesForLangChange = Cache::get('routesForLangChange');

        return response()->json([
            'allSections' => $allSections,
            'allRoutes' => $allRoutes,
            'allLangs' => $allLangs,
            'locale' => $locale,
            'routes_for_lang_change' => $routesForLangChange,
            ]);
    }
}
