<?php

namespace App\Http\Controllers;

use App\Lang;
use App\VariousData;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

/**
 * Class NavbarController
 * @package App\Http\Controllers
 */
class NavbarController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {

        $navbar_db = VariousData::locale($locale)->where('name', 'navbar')->get('properties')->first()->properties;

        return response()->json([
            'navbar' => $navbar_db,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(Request $request, $locale)
    {
        // обновляем только заголовок
        if ($request->has('navbar')) {

            $position = VariousData::locale($locale)->firstWhere('name', 'navbar');

            $position->properties = $request->navbar;
            $position->save();

            $response_object = [
                'status' => 'success',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['navbar']['messageUpdate'],
                ];
//                'message' => 'Navbar updated!'];
            $response_object['navbar'] = $position->properties;


            return response()->json($response_object, 200);

        } else {
            return response('', 500);
        }
    }




}
