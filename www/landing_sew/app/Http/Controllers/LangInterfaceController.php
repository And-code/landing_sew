<?php

namespace App\Http\Controllers;

use App\IconsGrid;
use App\ImageShowcase;
use App\Lang;
use App\Rules\ValidInterfaceKeys;
use App\Testimonial;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class LangInterfaceController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {

//        $interfaceLang = VariousData::locale($locale)->where(['name' => 'otherFields'])->get('properties')->first()->properties;

        $interfaceLang = Cache::get('allSections_' . app()->getLocale())['otherFields']['sectionData'];

        return response()->json([
            'interfaceLang' => $interfaceLang,
            'locale' => $locale,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale)
    {
        // валидация данных. Поле должно быть JSON формата
        $messages = [
            'required' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['validationMessages']['required'],
//            'required' => 'The :attribute field is required.',
//            'json' => 'The :attribute must be a valid JSON string.',
        ];

        $validator = Validator::make($request->all(), [
            'interfaceTranslationNew' => ['bail', 'required', new ValidInterfaceKeys()],
        ], $messages)->validate();

            $data = $request->interfaceTranslationNew;



            DB::transaction(function () use ($data, $locale) {
                $variousData = VariousData::locale($locale)->where(['name' => 'otherFields'])->get()->first();

                $variousData->properties = $data;
                $variousData->save();
            });

            $response_object = [
                'status' => 'success',
//                'message' => 'Lang ' . $data['langTo'] . ' added!'
                'message' => Cache::get('allSections_' . $locale)
                                    ['otherFields']['sectionData']['interface']['langInterface']['messageUpdate'] . "(". $locale . ").",
            ];

            return response()->json($response_object, 200);
    }

}
