<?php

namespace App\Http\Controllers;

use App\Helpers\HelperFunctions;
use App\IconsGrid;
use App\Lang;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;

class IconsGridController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        // проверка видимости секции
        $visibility_db = Visibility::locale($locale)->where('sectionName', 'icons_grids')->get('visibility')->first()->visibility;

        if ($visibility_db == true) {
//            $icons_grids_db = IconsGrid::get(['id_id AS id', 'iconName', 'headerValue', 'pValue'])->toArray();
            $icons_grids_db = IconsGrid::locale($locale)->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            return response()->json([
                'visibility' => true,
                'iconGrids' => $icons_grids_db,
            ]);

        } else {
            return response()->json([
                'visibility' => false,
//                'hiddenSectionValue' => 'Раздел "Icons Grids" скрыт',
                'hiddenSectionValue' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['iconGrid']['hiddenSectionValue'],
            ]);
        }

    }

    // получение списка иконок из css
    public function icons() {
        $path = 'vendor/simple-line-icons/css/simple-line-icons.css';
        $file = fopen($path, 'r');

        if ($file === false) {
            return response()->json([
                'icons'=> 'No data',
            ]);
        }

        $arr = [];
        while (!feof($file)) {
            $str = fgets($file);
            if (strpos($str, '.icon-') === 0) {
                $arr[] = (preg_split("/[.\s,:]+/", $str))[1];
            }
        }
        fclose($file);

        $uniqe_arr = array_unique($arr);

        foreach ($uniqe_arr as &$row) {
            $row = [
                'value' => $row,
                'text' => $row,
            ];
        }

        return response()->json([
            'icons'=> $uniqe_arr,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $locale)
    {
        if ($request->filled(['iconName', 'headerValue', 'pValue'])) {

            $data = $request->only(['iconName', 'headerValue', 'pValue']);

            $data['lang_id'] = Lang::where('lang', $locale)->first()->id;

            $icons_grids = IconsGrid::create($data);

            // получаем данные про все записи из базы данных
            $icons_grids_db = IconsGrid::locale($locale)->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'Icon grid added!'];
            'message' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['iconGrid']['messageAdd'],
                ];


            $response_object['iconGrids'] = $icons_grids_db;

            return response()->json($response_object, 201);
        } else {
            return response('', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $locale, $id)
    {
        if ($request->has(['iconName', 'headerValue', 'pValue'])) {

            $data = $request->only(['iconName', 'headerValue', 'pValue']);

//            $icons_grids = IconsGrid::locale($locale)->where('id', $id)->update($data);
            $icons_grids = IconsGrid::locale($locale)->where('id', $id)->first();
            $icons_grids_res = $icons_grids->update($data);

//            HelperFunctions::updateCacheForLocale($locale);

            // получаем данные про все записи из базы данных
            $icons_grids_db = IconsGrid::locale($locale)->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'Icon grid updated!',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['iconGrid']['messageUpdate'],
            ];

            $response_object['iconGrids'] = $icons_grids_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($locale, $id)
    {

        //        $deleted_count = IconsGrid::destroy($id);
        $deleted_count1 = IconsGrid::locale($locale)->where('id', $id)->first();
        $deleted_count = $deleted_count1->delete();

        if ($deleted_count > 0) {

            // получаем данные про все записи из базы данных
            $icons_grids_db = IconsGrid::locale($locale)->get(['id', 'iconName', 'headerValue', 'pValue'])->toArray();

            $response_object = [
                'status' => 'success',
//                'message' => 'deleted',
                'message' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['iconGrid']['messageDelete'],
            ];

            $response_object['iconGrids'] = $icons_grids_db;

            return response()->json($response_object, 200);
        } else {
            return response('', 500);
        }

    }
}
