<?php

namespace App\Http\Controllers;

use App\Lang;
use App\VariousData;
use App\Visibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;

class CallToActionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($locale)
    {
        // проверка видимости секции
        $visibility_db = Visibility::locale($locale)->where('sectionName', 'callToActions')->
                                        get('visibility')->first()->visibility;

        if ($visibility_db == true) {
            $callToActions_db = VariousData::locale($locale)->where('name', 'callToActions')->
                                            get('properties')->first()->properties;

            return response()->json([
                'visibility' => true,
                'callToActions' => $callToActions_db,
            ]);

        } else {
            return response()->json([
                'visibility' => false,
//                'hiddenSectionValue' => 'Раздел "callToActions" скрыт',
                'hiddenSectionValue' => Cache::get('allSections_' . $locale)
                                ['otherFields']['sectionData']['interface']['callToActions']['hiddenSectionValue'],
            ]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale)
    {

        $path = null;

        $position = VariousData::locale($locale)->where('name', 'callToActions')->get()->first();

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            // Обновляем картинку
            $nameOriginal = $request->file->getClientOriginalName();
            $newName = 'bg-calltoactions-' . $nameOriginal;

            // !!!! для правильной работы нужно в контейнере PHP docker в папке с laravel вызвать php artisan storage:link
            // сейчас настроено на прямое сохранение в public, а не storage.
            $path_save = $request->file->storeAs('/uploads/images', $newName);
            $path = '/uploads/images/' . $newName;

        }

        if ($request->has(['imgUrl', 'headerValue', 'placeholderValue', 'buttonValue'])) {

            // обновляем остальные поля
            $data = $request->only(['imgUrl', 'headerValue', 'placeholderValue', 'buttonValue']);

            if ($path != null) {
                $data['imgUrl'] = $path;
            }

            $position = VariousData::locale($locale)->where('name', 'callToActions')->get()->first();
            $position->properties = $data;
            $position->save();

            // удаляем предыдущую картинку если она не default
            if ($path != null &&
                $request->imgUrl != '/img/bg-masthead.jpg' &&
                $request->imgUrl != $path
            ) {
                $path_to_del = public_path() . $request->imgUrl;
                $res_bool = File::delete($path_to_del);
            }

            $response_object = [
                'status' => 'success',
//                'message' => 'callToActions updated!',
                'message' => Cache::get('allSections_' . $locale)
                            ['otherFields']['sectionData']['interface']['callToActions']['messageUpdate'],
                ];
            $response_object['callToActions'] = $position->properties;

            return response()->json($response_object, 200);

        } else {
            return response('', 500);
        }

    }


}
