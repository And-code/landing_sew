<?php


namespace App\Listeners;


use Illuminate\Support\Facades\Log;

class LogKeyForgotten
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param    $event
     * @return void
     */
    public function handle($event)
    {
        //

//        Log::info('Cache forgotten', json_encode($event)[$event->user_name => $event->article_name]);

//        if ($event->key === 'routesForLangChange') {
//            Log::info('Cache forgotten', ['Cache forgotten' => json_encode($event)]);
//        }

        Log::info('Cache forgotten', ['Cache forgotten' => json_encode($event)]);

    }
}
