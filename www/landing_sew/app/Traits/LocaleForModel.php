<?php


namespace App\Traits;

// Добавляет слушатели событий на удаление, обновление данных для конкретной локали.
// Добавляет локальный scope для модели для ограничения конкретной локалью

use App\Helpers\HelperFunctions;
use App\Lang;

trait LocaleForModel
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
//        static::updated(function ($thisModel) {
//            //
//            $lang = $thisModel->lang->lang;
//
//            HelperFunctions::updateCacheForLocale($lang);
//        });

        static::saved(function ($thisModel) {
            //
            $lang = $thisModel->lang->lang;

            HelperFunctions::updateCacheForLocale($lang);
        });

        static::deleted(function ($thisModel) {
            //
            $lang = $thisModel->lang->lang;

//            HelperFunctions::deleteCacheForLocale($lang);
            HelperFunctions::updateCacheForLocale($lang);
        });
    }

    /**
     * Scope a query to only include results for locale.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLocale($query, $locale)
    {
        $lang_id = Lang::firstWhere('lang', $locale)->id;

        return $query->where('lang_id', '=', $lang_id);
    }
}
