<?php

namespace App;

use App\Helpers\HelperFunctions;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LocaleForModel;

class VariousData extends Model
{

    use LocaleForModel;

    //
    protected $fillable = ['name', 'properties', 'lang_id'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'properties' => 'array',
    ];


    public function lang () {
        return $this->belongsTo('App\Lang');
    }
}
