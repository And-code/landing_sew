alias env-up='docker-compose up -d'
alias env-stop='docker-compose stop'
alias phpunit='docker-compose run --rm php74-first vendor/bin/phpunit'
alias php74first="cd ~/public_html/landing_sew && docker-compose exec php74-first bash"
