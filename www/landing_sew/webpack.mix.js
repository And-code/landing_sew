const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// if (mix.inProduction()) {
//     mix.version();
// }

// Bootstrap
// mix.copyDirectory('node_modules/bootstrap/dist', 'public/vendor/bootstrap');
// Font Awesome CSS
mix.copy('node_modules/@fortawesome/fontawesome-free/css/**/*', 'public/vendor/fontawesome-free/css');
// Font Awesome Webfonts
mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts/**/*', 'public/vendor/fontawesome-free/webfonts');
// jQuery Easing
// mix.copy('node_modules/jquery.easing/*.js', 'public/vendor/jquery-easing');
// jQuery
//// mix.copy(['node_modules/jquery/dist/*', '!node_modules/jquery/dist/core.js'], 'public/vendor/jquery');
// mix.copy('node_modules/jquery/dist/*', 'public/vendor/jquery');
// Simple Line Icons
mix.copy('node_modules/simple-line-icons/fonts/**', 'public/vendor/simple-line-icons/fonts');
mix.copy('node_modules/simple-line-icons/css/**', 'public/vendor/simple-line-icons/css');

// img copy
mix.copyDirectory('resources/img', 'public/img').version();

// CSS task
mix.sass('resources/scss/landing-page.scss', 'public/css', {
    sassOptions: {
        outputStyle: "expanded",
        includePaths: ["node_modules"],
    }
}).options({
    processCssUrls: false
})
    .minify('public/css/landing-page.css');
// mix.sass('resources/scss/landing-page.scss', 'public/css');




// // Bootstrap
// var bootstrap = gulp.src('./node_modules/bootstrap/dist/**/*')
//     .pipe(gulp.dest('./vendor/bootstrap'));
// Font Awesome CSS
// var fontAwesomeCSS = gulp.src('./node_modules/@fortawesome/fontawesome-free/css/**/*')
//     .pipe(gulp.dest('./vendor/fontawesome-free/css'));
// Font Awesome Webfonts
// var fontAwesomeWebfonts = gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/**/*')
//     .pipe(gulp.dest('./vendor/fontawesome-free/webfonts'));
// jQuery Easing
// var jqueryEasing = gulp.src('./node_modules/jquery.easing/*.js')
//     .pipe(gulp.dest('./vendor/jquery-easing'));
// jQuery
// var jquery = gulp.src([
//     './node_modules/jquery/dist/*',
//     '!./node_modules/jquery/dist/core.js'
// ])
//     .pipe(gulp.dest('./vendor/jquery'));
// Simple Line Icons
// var simpleLineIconsFonts = gulp.src('./node_modules/simple-line-icons/fonts/**')
//     .pipe(gulp.dest('./vendor/simple-line-icons/fonts'));
// var simpleLineIconsCSS = gulp.src('./node_modules/simple-line-icons/css/**')
//     .pipe(gulp.dest('./vendor/simple-line-icons/css'));
// return merge(bootstrap, fontAwesomeCSS, fontAwesomeWebfonts, jquery, jqueryEasing, simpleLineIconsFonts, simpleLineIconsCSS);
