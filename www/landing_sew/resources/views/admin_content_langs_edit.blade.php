@extends('layout_for_all_ext')

{{--@section('title', "Админ панель - ")--}}
@section('title', $title)

@section('content')
    @include('sections.navbar_admin')


{{--    имена атрибутов только с маленькой буквы--}}

    <allblocks-data-langs-component
        v-bind:sections="{{ $sections }}"
        v-bind:routes="{{ $routes }}"
        v-bind:langs="{{ $langs }}"
        v-bind:routes_for_lang_change="{{ $routes_for_lang_change }}"
        locale="{{ $locale }}"
        v-bind:interface="{{ json_encode($interface) }}">
    </allblocks-data-langs-component>
@endsection


{{--        v-bind:routesForLangChange="{{ $routesForLangChange }}"--}}
