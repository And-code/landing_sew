@extends('layout_for_all_ext')

@section('title', $title)

@section('content')

    <allblocks-info-component
        v-bind:pagetype="`{{ $pagetype }}`"
        v-bind:sections="{{ $sections }} "
        v-bind:routes="{{ $routes }}"
        locale="{{ $locale }}"
    >
    </allblocks-info-component>

@endsection



