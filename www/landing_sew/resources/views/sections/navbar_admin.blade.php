
{{--@section('navbar')--}}
<!-- Navigation -->

<nav class="navbar navbar-expand navbar-light bg-light static-top">
{{--    <div class="container d-flex flex-row-reverse">--}}
    <div class="container d-flex flex-row">
{{--    <div class="container">--}}

{{--        <a class="navbar-brand" href="{{ route('index') }}">--}}
{{--            {{ App\VariousData::where('name', 'navbar')->get('properties')->first()->properties['siteName'] }}--}}
{{--        </a>--}}
        <a class="navbar-brand" href="{{ route('land.index') }}">
{{--            На главную--}}
            {{ cache('allSections_' . app()->getLocale())['navbar']['sectionData']['siteName'] }}
        </a>



        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
{{--                <a class="nav-link" href="{{ route('land.indexAdmin') }}">{{ __('Main') }}</a>--}}
                <a class="nav-link" href="{{ route('land.indexAdmin') }}">
{{--                    {{ cache('allSections_' . app()->getLocale())['otherFields']['sectionData']['main'] }}--}}
                    {{ $interface['main'] }}
                </a>
            </li>
            <li class="nav-item">
{{--                <a class="nav-link" href="{{ route('land.indexFooterEdit') }}">{{ __('Footer') }}</a>--}}
                <a class="nav-link" href="{{ route('land.indexFooterEdit') }}">
{{--                    {{ cache('allSections_' . app()->getLocale())['otherFields']['sectionData']['footer'] }}--}}
                    {{ $interface['footer'] }}
                </a>
            </li>
            <li class="nav-item">
{{--                <a class="nav-link" href="{{ route('land.indexContentLangsEdit') }}">{{ __('Languages') }}</a>--}}
                <a class="nav-link" href="{{ route('land.indexContentLangsEdit') }}">
{{--                    {{ cache('allSections_' . app()->getLocale())['otherFields']['sectionData']['languages'] }}--}}
                    {{ $interface['languages'] }}
                </a>
            </li>
            <li class="nav-item">
                {{--                <a class="nav-link" href="{{ route('land.indexContentLangsEdit') }}">{{ __('Languages') }}</a>--}}
                <a class="nav-link" href="{{ route('land.indexInterfaceLangsEdit') }}">
                    {{--                    {{ cache('allSections_' . app()->getLocale())['otherFields']['sectionData']['languages'] }}--}}
                    {{ $interface['interfaceTranslation'] }}
                </a>
            </li>
        </ul>

        <form class="form-inline" action="{{ route('land.logout')  }}" method="POST">

            @csrf
{{--            <button type="submit" class="btn btn-danger">{{ __('Logout') }}</button>--}}
            <button type="submit" class="btn btn-danger">
{{--                {{ cache('allSections_' . app()->getLocale())['otherFields']['sectionData']['logout'] }}--}}
{{--                {{ $interface['logout'] }}--}}
                {{ $interface['interface']['auth']['logout'] }}
            </button>

        </form>
    </div>

</nav>


