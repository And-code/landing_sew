@extends('layout_for_all_ext')

{{--@section('title', "Админ панель - ")--}}
@section('title', $title)

@section('content')
    @include('sections.navbar_admin')


{{--    имена атрибутов только с маленькой буквы--}}

    <allblocks-interface-langs-component
        v-bind:sections="{{ $sections }}"
        v-bind:routes="{{ $routes }}"
        locale="{{ $locale }}"
        v-bind:interface="{{ json_encode($interface) }}">
    </allblocks-interface-langs-component>
@endsection


{{--        v-bind:routesForLangChange="{{ $routesForLangChange }}"--}}
