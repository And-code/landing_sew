@extends('layout_for_all_ext')

{{--@section('title', "Админ панель - ")--}}
@section('title', $title)

@section('content')
    @include('sections.navbar_admin')

    <allblocks-footer-component
        v-bind:sections="{{ $sections }} "
        v-bind:routes="{{ $routes }}"
        v-bind:interface="{{ json_encode($interface) }}">
    </allblocks-footer-component>
@endsection
