@extends('layout_for_all_ext')

@section('title', $title)

@section('content')
    @include('sections.navbar_admin')

    <allblocks-admin-component
        v-bind:sections="{{ $sections }} "
        v-bind:routes="{{ $routes }}"
        locale="{{ $locale }}"
        v-bind:interface="{{ json_encode($interface) }}">
    </allblocks-admin-component>
@endsection

