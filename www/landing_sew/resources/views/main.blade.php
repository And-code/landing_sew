@extends('layout_for_all_ext')

@section('title', $title)

@section('content')

    <allblocks-main-component
        v-bind:sections="{{ $sections }} "
        v-bind:routes="{{ $routes }}"
        locale="{{ $locale }}"
    >
    </allblocks-main-component>

@endsection



