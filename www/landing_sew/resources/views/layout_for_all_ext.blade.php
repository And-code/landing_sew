<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<!-- Bootstrap core CSS -->
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin panel">
    <meta name="author" content="and1">

{{--    <title>@yield('title') {{ App\VariousData::where('name', 'navbar')->get('properties')->first()->properties['siteName'] }}</title>--}}
    <title>@yield('title')</title>


    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
          type="text/css">



    <!-- Custom styles for this template -->
    <link href="/css/landing-page.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

{{--    <!-- Fonts -->--}}
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}


</head>

<body>
<div id="app">

    @yield('content')

</div>


<!-- Bootstrap core JavaScript -->
{{--<script src="/vendor/jquery/jquery.min.js"></script>--}}
{{--<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--}}

<script>


</script>
</body>

</html>
