/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// Install BootstrapVue
Vue.use(BootstrapVue);
// // Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin)

// import { BAlert } from 'bootstrap-vue';
// Vue.component('b-alert', BAlert);
//
// import { BModal, VBModal } from 'bootstrap-vue';
// Vue.component('b-modal', BModal);
// Vue.directive('b-modal', VBModal);
//
// import { BForm } from 'bootstrap-vue';
// Vue.component('b-form', BForm);
//
// import { BFormCheckbox } from 'bootstrap-vue';
// Vue.component('b-form-checkbox', BFormCheckbox);
//
// import { BFormFile } from 'bootstrap-vue';
// Vue.component('b-form-file', BFormFile);
//
// import { BFormGroup } from 'bootstrap-vue';
// Vue.component('b-form-group', BFormGroup);
//
// import { BFormInput } from 'bootstrap-vue';
// Vue.component('b-form-input', BFormInput);
//
// import { BFormSelect } from 'bootstrap-vue';
// Vue.component('b-form-select', BFormSelect);
//
// import { BButton } from 'bootstrap-vue';
// Vue.component('b-button', BButton);

// And import Bootstrap and BootstrapVue css files:

// bootstrap.css загружается в другом месте
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//vue-wysiwyg
import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {
    // if the image option is not set, images are inserted as base64
    // image: {
    //     uploadURL: "/ru/admin/footer_upload_image",
    //     dropzoneOptions: {}
    // },
});
import 'vue-wysiwyg/dist/vueWysiwyg.css';


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('alert', require('./components/Alert.vue').default);
Vue.component('allblocks-admin-component', require('./components/AllBlocksAdminComponent.vue').default);
Vue.component('allblocks-main-component', require('./components/AllBlocksMainComponent.vue').default);
Vue.component('allblocks-footer-component', require('./components/AllBlocksFooterComponent.vue').default);
Vue.component('allblocks-info-component', require('./components/AllBlocksInfoComponent.vue').default);

Vue.component('allblocks-interface-langs-component', require('./components/AllBlocksInterfaceLangsComponent.vue').default);
Vue.component('interface-lang-edit-ext-component', require('./components/extAdmin/InterfaceLangEditExtComponent.vue').default);

Vue.component('modal-ext-component', require('./components/extAdmin/ModalExtComponent.vue').default);

Vue.component('allblocks-data-langs-component', require('./components/AllBlocksDataLangsComponent.vue').default);
Vue.component('langs-data-add-ext-component', require('./components/extAdmin/LangsDataAddExtComponent.vue').default);



Vue.component('navbar-ext-main-component', require('./components/extMain/NavbarBlockExtComponent.vue').default);
Vue.component('header-ext-main-component', require('./components/extMain/HeaderExtComponent.vue').default);
Vue.component('icon-grid-block-ext-main-component', require('./components/extMain/IconGridBlockExtComponent.vue').default);
Vue.component('image-showcases-ext-main-component', require('./components/extMain/ImageShowcasesExtMainComponent.vue').default);
Vue.component('image-showcases-block-ext-main-component', require('./components/extMain/ImageShowcasesBlockExtComponent.vue').default);
Vue.component('icon-grid-ext-main-component', require('./components/extMain/IconGridExtMainComponent.vue').default);
Vue.component('testimonials-ext-main-component', require('./components/extMain/TestimonailsExtMainComponent.vue').default);
Vue.component('testimonials-block-ext-main-component', require('./components/extMain/TestimonailsBlockExtComponent.vue').default);
Vue.component('calltoactions-ext-main-component', require('./components/extMain/CallToActionsExtComponent.vue').default);
Vue.component('footer-ext-main-component', require('./components/extMain/FooterBlockExtComponent.vue').default);
Vue.component('footer-edit-ext-component', require('./components/extAdmin/FooterEditExtComponent.vue').default);
Vue.component('info-ext-component', require('./components/extMain/InfoExtComponent.vue').default);


Vue.component('navbar-component', require('./components/old/NavbarBlockComponent.vue').default);
Vue.component('navbar-ext-admin-component', require('./components/extAdmin/NavbarBlockExtComponent.vue').default);
Vue.component('header-ext-admin-component', require('./components/extAdmin/HeaderExtComponent.vue').default);
Vue.component('icon-grid-block-ext-admin-component', require('./components/extAdmin/IconGridBlockExtComponent.vue').default);
Vue.component('image-showcases-block-ext-admin-component', require('./components/extAdmin/ImageShowcasesBlockExtComponent.vue').default);
Vue.component('testimonials-block-ext-admin-component', require('./components/extAdmin/TestimonailsBlockExtComponent.vue').default);
Vue.component('calltoactions-ext-admin-component', require('./components/extAdmin/CallToActionsExtComponent.vue').default);

Vue.component('footer-block-ext-admin-component', require('./components/extAdmin/FooterBlockExtComponent.vue').default);

Vue.component('header-component', require('./components/old/HeaderComponent.vue').default);
Vue.component('calltoactions-component', require('./components/old/CallToActionsComponent.vue').default);


Vue.component('icon-grid-component', require('./components/extAdmin/IconGridComponent.vue').default);
Vue.component('icon-grid-block-component', require('./components/old/IconGridBlockComponent.vue').default);
Vue.component('image-showcases-component', require('./components/extAdmin/ImageShowcasesComponent.vue').default);
Vue.component('image-showcases-block-component', require('./components/old/ImageShowcasesBlockComponent.vue').default);
Vue.component('testimonials-component', require('./components/extAdmin/TestimonailsComponent.vue').default);
Vue.component('testimonials-block-component', require('./components/old/TestimonailsBlockComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',



});
