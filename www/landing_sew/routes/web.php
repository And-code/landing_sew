<?php

use App\Lang;
use App\VariousData;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/icons', function () {
////   $path = 'vendor/simple-line-icons/css/simple-line-icons.css';
////    $file = fopen($path, 'r')
////                or die('Unable to open file "/vendor/simple-line-icons/css/simple-line-icons.css"');
////
////    $arr = [];
////    while (!feof($file)) {
////        $str = fgets($file);
////        if (strpos($str, '.icon-') === 0) {
////
////            $sub_str = (preg_split("/[.\s,:]+/", $str))[1];
////            $arr[] = $sub_str;
////        }
////    }
////   fclose($file);
////
////    echo 'Размер неуникальный: ' . sizeof($arr) .' <br>';
////    echo '<pre>';
////    print_r($arr);
////    echo '</pre>';
////
////    $uniqe_arr = array_unique($arr);
////    echo 'Размер уникальный: ' . sizeof($uniqe_arr) . '<br>';
////    echo '<pre>';
////    print_r($uniqe_arr);
////    echo '</pre>';
//
//    $path = 'vendor/simple-line-icons/css/simple-line-icons.css';
//    $file = fopen($path, 'r');
//
//    if ($file === false) {
//        return response()->json([
//            'incons'=> 'No data',
//        ]);
//    }
//
//    $arr = [];
//    while (!feof($file)) {
//        $str = fgets($file);
//        if (strpos($str, '.icon-') === 0) {
//            $arr[] = (preg_split("/[.\s,:]+/", $str))[1];
//        }
//    }
//    fclose($file);
//
//    $uniqe_arr = array_unique($arr);
//
//    foreach ($uniqe_arr as &$row) {
//        $row = [
//            'value' => $row,
//            'text' => $row,
//        ];
//    }
//
//    return response()->json([
//        'incons'=> $uniqe_arr,
//    ]);
//
//
////    return response('icons...' . $file);
//});


//Route::get('/icons-social', function () {
//    $path = 'vendor/fontawesome-free/css/all.css';
//    $file = fopen($path, 'r');
//
//    if ($file === false) {
//        return response()->json([
//            'incons-social'=> 'No data',
//        ]);
//    }
//
//    $arr = [];
//    while (!feof($file)) {
//        $str = fgets($file);
//        if (strpos($str, '.fa-') === 0 &&
//            strpos($str, ':before') !== false) {
//            $arr[] = (preg_split("/[.:]+/", $str))[1];
//        }
//    }
//    fclose($file);
//
//    $uniqe_arr = array_unique($arr);
//
//    // преобразование массива
//    foreach ($uniqe_arr as &$row) {
//        $row = [
//            'value' => $row,
//            'text' => $row,
//        ];
//    }
//
////    echo 'Размер неуникальный: ' . sizeof($arr) .' <br>';
////    echo '<pre>';
////    print_r($arr);
////    echo '</pre>';
////
////    $uniqe_arr = array_unique($arr);
////    echo 'Размер уникальный: ' . sizeof($uniqe_arr) . '<br>';
////    echo '<pre>';
////    print_r($uniqe_arr);
////    echo '</pre>';
////
//    return response()->json([
//        'incons'=> $uniqe_arr,
//    ]);
//
//});

Route::get('/trans', function () {

    $locale = 'en';

    echo '<pre>';
    print_r(\Illuminate\Support\Facades\Cache::get('allSections_' . $locale) );
    echo '</pre>';
///
});


Route::get('/routes', function (){



    $routes =  Route::getRoutes();

    $locale = app()->getLocale();

    $arr = [];
    foreach ($routes as $route) {

//        $route->hasParameter('locale') ? $route->setParameter('locale', $locale) : "";
//
//        echo json_encode($route->parameters) . "<br>";

//

//        echo 'position : ' . strpos($route->getName(), 'land.');


        if (strpos($route->getName(), 'land.') !== false &&
//            in_array('GET', $route->methods())
            (in_array('GET', $route->methods()) || strpos($route->getName(), 'visibility.update') !== false)
        ) {
            $url = route("" . $route->getName(), ['locale' => $locale ]);
            echo $url . "<br>";

//            $arr[] = [
//                "url" => $url_,
//                'routeName' => $route->getName(),
//            ];

            $arr[$route->getName()] =  $url;

//            echo $route->uri . " : " .
//                $route->getName() ." : ".
//                json_encode($route->methods()) .
//                "<br>";
        }
    }

    $tempArr = [];

    $url = url('/');

    $langs = \App\Lang::get('lang')->toArray();

//    $langs2  = \Illuminate\Support\Arr::get($langs, "*.lang");

    foreach ($langs as $lang) {
        $tempArr[$lang['lang']] = $url . "/". $lang['lang'];
    }
//
//    $langs = \App\Lang::get('lang')->each(function ($item, $key) use ($tempArr) {
//        $tempArr[$item->lang] = url('/') . "/". $item->lang;
//    });

    $arr['langs.routes'] = $tempArr;

    echo '<pre>';
    print_r($arr);
    echo '</pre>';
});

Route::get('/select', function () {

//    $lang_id = Lang::where('lang', $locale)->first()->id;
//
//    $position = VariousData::where(['name' => 'navbar', 'lang_id' => $lang_id])->get()->first();

//    $lang_id = Lang::where('lang', $locale)->first()->id;

//    $position = VariousData:: where(['name' => 'navbar'])->where(['lang_id' => Lang::firstWhere('lang', 'en')->id])->toSql();

    $navbar_db = VariousData::where('name', 'navbar')->get('properties')->first()->properties;
    $navbar_db2 = VariousData::with('lang')->where('name', 'navbar')->get('properties')->first()->properties;


//    $position = VariousData::select('properties')->firstWhere(['name' => 'navbar', 'lang_id' => 1])->properties;
//    $position2 = VariousData::select('properties')->
//                    firstWhere(['name' => 'navbar', 'lang_id' => Lang::firstWhere('lang', 'en')->id])->properties;
////    $position = VariousData::firstWhere(['name' => 'navbar', 'lang_id' => 1]);
//
//    print_r($position);
//    echo "<br>";
//    print_r($position2);

});


Route::get('/', ["uses"=>"MainPageController@redirectToLocation", "as"=>"redirectToLocation"]);

Route::group(['prefix'=>'{locale}',
                'where'=>['locale' => '[a-zA-Z]{2}'],
                'as'=>'land.',
//                'middleware' => ['setLocale', 'setRouteLocale', 'setCache']], function ($locale) {
                'middleware' => ['setLocale', 'setDefaultLocaleForUrls', 'setCache']], function ($locale) {

    // стандартные маршруты аутентификации без сброса пароля и проверки email
    Auth::routes(['reset' => false, 'verify' => false, ]);
//    Auth::routes(['verify' => false, ]);

    Route::get('/', ["uses"=>"MainPageController@show", "as"=>"index"]);

    Route::get('/about', ["uses"=>"MainPageController@about", "as"=>"about"]);
    Route::get('/contact', ["uses"=>"MainPageController@contact", "as"=>"contact"]);
    Route::get('/terms_of_use', ["uses"=>"MainPageController@termsOfUse", "as"=>"terms_of_use"]);
    Route::get('/privacy_policy', ["uses"=>"MainPageController@privacyPolicy", "as"=>"privacy_policy"]);


    Route::get('/alldata', ["uses"=>"IndexController@index", "as"=>"indexAlldata"]);

    Route::group(['prefix'=>'admin',
        'middleware' => ['auth']], function () {

        Route::get('/', ["uses"=>"AdminController@show", "as"=>"indexAdmin"]);
        Route::get('/edit_footer', ["uses"=>"AdminController@showFooterEdit", "as"=>"indexFooterEdit"]);
        Route::get('/edit_content_langs', ["uses"=>"AdminController@showContentLangsEdit", "as"=>"indexContentLangsEdit"]);
        Route::get('/edit_interface_langs', ["uses"=>"AdminController@showInterfaceLangsEdit", "as"=>"indexInterfaceLangsEdit"]);

        Route::put('/edit_interface_langs', ["uses"=>"LangInterfaceController@update", "as"=>"langInterface.update"]);

        Route::get('/footer', ["uses"=>"FooterController@index", "as"=>"footer.index"]);
        Route::put('/footer', ["uses"=>"FooterController@update", "as"=>"footer.update"]);
        Route::get('/footer/icons_social', ["uses"=>"FooterController@iconsSocial", "as"=>"footer.iconsSocial"]);
//        Route::post('/footer_upload_image', ["uses"=>"FooterController@uploadWysiwygImage", "as"=>"footer.uploadWysiwygImage"]);


        Route::resource('gridicons', 'IconsGridController')->only([
            'index', 'update', 'store', 'destroy'
        ]);

        Route::get('gridicons/icons', ["uses" => 'IconsGridController@icons', "as" => 'icons']);


        Route::resource('imageshowcases', 'ImageShowcasesController')->only([
           'index', 'update', 'store', 'destroy'
        ]);

        Route::resource('testimonials', 'TestimonialsController')->only([
            'index', 'update', 'store', 'destroy'
        ]);

        Route::resource('langs', 'LangController')->only([
            'index', 'update', 'store', 'destroy'
        ]);

        Route::put('/add_lang', ["uses"=>"LangController@update", "as"=>"navbar.update"]);

        Route::get('/navbar', ["uses"=>"NavbarController@index", "as"=>"navbar.index"]);
        Route::put('/navbar', ["uses"=>"NavbarController@update", "as"=>"navbar.update"]);

        Route::get('/header', ["uses"=>"HeaderController@index", "as"=>"header.index"]);
        Route::put('/header', ["uses"=>"HeaderController@update", "as"=>"header.update"]);

        Route::get('/calltoactions', ["uses"=>"CallToActionsController@index", "as"=>"calltoactions.index"]);
        Route::put('/calltoactions', ["uses"=>"CallToActionsController@update", "as"=>"calltoactions.update"]);

//        Route::get('/visibility', ["uses"=>"VisibilityController@index", "as"=>"visibility.index"]);
        Route::put('/visibility', ["uses"=>"VisibilityController@update", "as"=>"visibility.update"])
            ->middleware('checkVisibiltyParams');



        // тут:
//        GET 	        /photos 	                index 	    photos.index
//        GET 	        /photos/create 	            create 	    photos.create
//        POST 	        /photos 	                store 	    photos.store
//        GET 	        /photos/{photo} 	        show 	    photos.show
//        GET 	        /photos/{photo}/edit 	    edit 	    photos.edit
//        PUT/PATCH 	/photos/{photo} 	        update 	    photos.update
//        DELETE 	    /photos/{photo} 	        destroy 	photos.destroy


    });



});



//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
