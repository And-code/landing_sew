<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;

class IconsGridSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('icons_grids')->insert([
            [
                'iconName' => 'icon-screen-desktop',
                'headerValue' => 'Fully Responsive',
                'pValue' => 'This theme will look great on any device, no matter the size!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
            [
                'iconName' => 'icon-layers',
                'headerValue' => 'Bootstrap 4 Ready',
                'pValue' => 'Featuring the latest build of the new Bootstrap 4 framework!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'iconName' => 'icon-check',
                'headerValue' => 'Easy to Use',
                'pValue' => 'Ready to use with your own content, or customize the source files!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
        ]);

    }
}
