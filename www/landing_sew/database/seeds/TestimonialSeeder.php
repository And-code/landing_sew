<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testimonials')->insert([
            [
                'imgUrl' => '/img/testimonials-1.jpg',
                'headerValue' => 'Margaret E.',
                'pValue' => '"This is fantastic! Thanks so much guys!"',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'imgUrl' => '/img/testimonials-2.jpg',
                'headerValue' => 'Fred S.',
                'pValue' => '"Bootstrap is amazing. I\'ve been using it to create lots of super nice landing pages."',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'imgUrl' => '/img/testimonials-3.jpg',
                'headerValue' => 'Sarah W.',
                'pValue' => '"Thanks so much for making these free resources available to us!"',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);



    }
}
