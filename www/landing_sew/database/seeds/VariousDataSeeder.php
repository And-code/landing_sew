<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

class VariousDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('various_data')->insert([
            [   'name' => 'navbar',
                'properties' => json_encode([
                    'siteName' => 'SiteLogo!',
                    'buttonName' => 'Sign In'
                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'header',
                'properties' => json_encode([
                    'imgUrl' => '/img/bg-masthead.jpg',
                    'headerValue' => 'Build a landing page for your business or project and generate more leads!',
                    'placeholderValue' => 'Enter your email...',
                    'buttonValue' => 'Sign up!',
                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'callToActions',
                'properties' => json_encode([
                    'imgUrl' => '/img/bg-masthead.jpg',
                    'headerValue' => 'Ready to get started? Sign up now!',
                    'placeholderValue' => 'Enter your email...',
                    'buttonValue' => 'Sign up!',
                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'testimonials',
                'properties' => json_encode([
                    'mainHeaderValue' => 'What people are saying...',
                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'footer',
                'properties' => json_encode([
                    'footerLeft' => [
                        'about' => [
                            'name' => 'About',
                            'text' => 'About text...',
                            'routeName' => 'land.about',
                            'visibility' => true,
                        ],
                        'contact' => [
                            'name' => 'Contact',
                            'text' => 'Contact text...',
                            'routeName' => 'land.contact',
                            'visibility' => true,
                        ],
                        'termsOfUse' => [
                            'name' => 'Terms of Use',
                            'text' => 'Terms of Use text...',
                            'routeName' => 'land.terms_of_use',
                            'visibility' => true,
                        ],
                        'privacyPolicy' => [
                            'name' => 'Privacy Policy',
                            'text' => 'Privacy Policy text...',
                            'routeName' => 'land.privacy_policy',
                            'visibility' => true,
                        ],
                    ],
                    'footerLeftP' => [
                        'pValue' => [
                            'text' => '&copy; Your Website 2020. All Rights Reserved.',
                            'visibility' => true,
                        ],
                    ],
                    'footerRight' => [
                        0 => [
                            'iconClassName' => 'fa-facebook',
                            'href' => '#',
                            'visibility' => true,
                        ],
                        1 => [
                            'iconClassName' => 'fa-twitter-square',
                            'href' => '#',
                            'visibility' => true,
                        ],
                        2 => [
                            'iconClassName' => 'fa-instagram',
                            'href' => '#',
                            'visibility' => true,
                        ],
                    ],
                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'otherFields',
                'properties' => json_encode([
                    'adminPageTitlePrefix' => 'Admin panel - ',

                    "main" => "Главная",
                    "footer" => "Футер",
                    "languages" => "Языки",
                    "interfaceTranslation" => "Интерфейс",

                    "loadingMsg" => "Загрузка...",
                    "noServerData" => "Нет данных от сервера...",

                    'interface' => [
                        'auth' => [
                            "login" => "Вход",
                            "register" => "Регистрация",
                            "logout" => "Выход",

                            "emailAddress" => "E-Mail адресс",
                            "password" => "Пароль",
                            "rememberMe" => "Запомнить меня",
                            "forgotYourPassword" => "Забыли пароль?",
                            "name" => "Имя",
                            "confirmPassword" => "Подтвердить пароль",
                        ],

                        'imageShowcase' => [
                            'buttonAdd' => "Добавить новый Image Showcase",

                            'messageErrorVisibility' => "Ошибка! Видимость не обновлена.",
                            'messageErrorAdd' => "Ошибка сервера! Элемент Image showcase не добавлен",
                            'messageErrorDelete' => "Ошибка! Позиция не удалена.",
                            'messageErrorUpdate' => "Ошибка! Позиция не обновлена.",

                            'messageAdd' => 'Image Showcase добавлен!',
                            'messageUpdate' => "Image Showcase обновлен!",
                            'messageDelete' => "Image Showcase удален!",

                            'hiddenSectionValue' => "Раздел imageShowcase скрыт",
                        ],

                        'header' => [

                            'messageErrorVisibility' => "Ошибка! Видимость не обновлена.",
                            'messageErrorUpdate' => "Ошибка! Header не обновлен.",

                            'messageUpdate' => "Header обновлен!",

                            'hiddenSectionValue' => "Раздел Header скрыт",
                        ],

                        'callToActions' => [

                            'messageErrorVisibility' => "Ошибка! Видимость не обновлена.",
                            'messageErrorUpdate' => "Ошибка! CallToActions не обновлен.",

                            'messageUpdate' => "CallToActions обновлен!",

                            'hiddenSectionValue' => "Раздел CallToActions скрыт",
                        ],

                        'navbar' => [
                            'messageErrorUpdate' => "Ошибка! Navbar не обновлен.",
                            'messageUpdate' => "Navbar обновлен!",
                        ],

                        'footer' => [
                            'linkFooterEdit' => "Редактировать футер",

                            'buttonSubmit' => "Обновить",

                            'labelNewName' => "Новое название ",
                            'placeholderNewName' => "Введите, например, ",

                            'checkboxPageVisibility' => "Показывать",

                            'labelCopyright' => "Название 'Copyright':",
                            'placeholderCopyright' => "Введите 'Copyright', например",

                            'labelIconSocial' => "Иконка ",
                            'placeholderIconSocial' => "Введите, например, ",

                            'prependVisibilitySocial' => "Показывать",
                            'placeholderLinkIconSocial' => "Введите ссылку на социальную сеть",

                            'messageErrorUpdate' => "Ошибка! позиция не обновлена.",
                            'messageUpdate' => "Footer обновлен.",
                        ],

                        'lang' => [
                            'numberTh' => "№",
                            'langTh' => "Язык",
                            'visibiblityTh' => "Показывать",
                            'actionTh' => "Действие",
                            'currentLocaleTh' => "Текущая локаль",

                            'buttonDelete' => "Удалить",
                            'pUnselectToDelete' => "Для удаления выберите другую локаль",
                            'buttonChose' => "Выбрать",
                            'isSelectedLocale' => "Да",

                            'prependCopyFromLocale' => "Копировать с ",
                            'prependCopyToLocale' => "в",

                            'placeholderLocaleEnter' => "например, 'ru'",
                            'buttonAdd' => "Добавить",
                            'bFormInvalidFeedback' => "Введите два латинских символа!",

                            'messageErrorVisibility' => "Ошибка! Видимость языка не обновлена.",
                            'messageErrorAdd' => "Ошибка сервера! Язык не добавлен",
                            'messageErrorDelete' => "Ошибка! Язык не удален",
                            'messageErrorDeleteLastLang' => "Ошибка! Последий язык нельзя удалить ",
                            'messageErrorUpdate' => "Ошибка! Видимость языка не обновлена.",

                            'messageErrorDublicate' => "Ошибка! Язык уже существует.",

                            'messageAdd' => 'Язык добавлен ',
                            'messageUpdate' => "Язык обновлен ",
                            'messageDelete' => "Язык удален ",

                        ],

                        'iconGrid' => [

                            'buttonAdd' => "Добавить элемент",

                            'messageErrorVisibility' => "Ошибка! Видимость не обновлена.",
                            'messageErrorAdd' => "Ошибка сервера! Элемент IconGrid не добавлен",
                            'messageErrorDelete' => "Ошибка! Позиция не удалена.",
                            'messageErrorUpdate' => "Ошибка! Позиция не обновлена.",

                            'messageAdd' => 'IconGrid добавлен!',
                            'messageUpdate' => "IconGrid обновлен!",
                            'messageDelete' => "IconGrid удален!",

                            'hiddenSectionValue' => "Раздел IconGrid скрыт",
                        ],

                        'testimonial' => [

                            'buttonAdd' => "Добавить новый Testimonial",
                            'buttonShowHideHeader' => "Показать/Скрыть заголовок",

                            'messageErrorVisibility' => "Ошибка! Видимость не обновлена.",
                            'messageErrorAdd' => "Ошибка сервера! Элемент Testimonial не добавлен",
                            'messageErrorDelete' => "Ошибка! Позиция не удалена.",
                            'messageErrorUpdate' => "Ошибка! Позиция не обновлена.",

                            'messageErrorUpdateHeader' => "Ошибка! Заголовок не обновлен.",

                            'messageAdd' => 'Testimonial добавлен!',
                            'messageUpdate' => "Testimonial обновлен!",
                            'messageUpdateHeader' => "Заголовок Testimonial обновлен!",
                            'messageDelete' => "Testimonial удален!",

                            'hiddenSectionValue' => "Раздел Testimonial скрыт",
                        ],

                        'langInterface' => [
                            'textareaName' => "Поля интерфейса в JSON формате",
                            'placeholderInterface' => "Тут должны быть поля интерфейса и их значения в формате JSON",
                            'buttonSubmit' => 'Сохранить',

                            'messageErrorUpdate' => "Ошибка! Язык интерфейса не обновлен.",
                            'messageUpdate' => "Язык интерфейса обновлен ",

                            'becomeNotArray' => " (стало не Array). Должно быть: ",
                            'mustPresentInJson' => "Ключ должен присутствовать в JSON",
                            'mustBeString' => "(Должен быть String! Стало Array в новом JSON?)",
                            'valueOfKeyMustBe' => "Ключ должен иметь значение: ",


                        ],
                        'validationMessages' => [
                            'required' => 'Поле :attribute обязательно.',
                            'jsonError' => 'Ошибка валидации json.'
                        ],

                        'buttonShowHideSection'=> "Показать/Скрыть секцию",
                    ],



                    'modal' => [
                        'commonImageChose' => [
                            'labelImageUrl' => 'URL картинки:',
                            'placeholderChoseFile' => 'Выберите файл картинки или перетащите сюда..',
                            'dropplaceholderChoseFile' => 'Перетащите сюда картинку...',
                            'currentFile' => 'Текущий файл:',
                            'selectedFile' => 'Выбранный файл:',
                            'unselectFile' => 'Очистить',
                        ],
                        'commonInputHeader' => [
                            'labelHeaderValue' => 'Заголовок:',
                            'placeholderHeaderValue' => 'Введите заголовок',
                        ],
                        'commonInputP' => [
                            'labelPInput' => 'Параграф:',
                            'placeholderPInput' => 'Введите параграф',
                        ],
                        'commonModalButtons' => [
                            'buttonSubmit' => 'ОК',
                            'buttonReset' => 'Отмена',
                            'buttonDelete' => 'Удалить',
                        ],

                        'commonInputPlaceholder' => [
                            'labelInputPlaceholder' => 'Метка-заполнитель (placeholder):',
                            'placeholderInputPlaceholder' => 'Введите метку-заполнитель',
                        ],
                        'commonButtonName' => [
                            'labelButtonName' => 'Название кнопки:',
                            'placeholderButtonName' => 'Введите название кнопки',
                        ],

                        'editNavbarModal' => [
                            'title' => [
                                'editNavbar' => 'Редактировать секцию Navbar',
                            ],
                            'labelEnterSiteName' => "Название сайта:",
                            'placeholderEnterSiteName' => "Введите название сайта",
                        ],

                        'editHeaderModal' => [
                            'title' => [
                                'editHeader' => 'Редактировать секцию header',
                            ],
                        ],

                        'imageShowcaseModal' => [
                            'title' => [
                                'addImageShowcase' => 'Добавить строку image showcase',
                                'editImageShowcase' => 'Редактировать строку image showcase',
                            ],
                        ],

                        'iconGridModal' => [
                            'title' => [
                                'addIconGrid' => 'Добавить IconGrid',
                                'editIconGrid' => 'Редактировать IconGrid',
                            ],
                            'labelIconName' => "название иконки:",
                        ],


                        'testimonialModal' => [
                            'title' => [
                                'addTestimonial' => 'Добавить Testimonial',
                                'editTestimonial' => 'Редактировать Testimonial',
                            ],
                        ],

                        'testimonialHeaderModal' => [
                            'title' => [
                                'editTestimonialHeader' => 'Редактировать Testimonial Header',
                            ],
                        ],

                        'callToActionsModal' => [
                            'title' => [
                                'editCallToActions' => 'Редактировать CallToActions',
                            ],
                        ],
                    ],


                ]),
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);

    }
}
