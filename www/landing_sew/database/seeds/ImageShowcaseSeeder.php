<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;

class ImageShowcaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('image_showcases')->insert([
//            [ 'id_id' => 0,
            [
                'imgUrl' => '/img/bg-showcase-1.jpg',
                'headerValue' => 'Fully Responsive Design',
                'pValue' => 'When you use a theme created by Start Bootstrap,
                            you know that the theme will look great on any device,
                            whether it\'s a phone, tablet, or desktop the page
                            will behave responsively!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
//            [ 'id_id' => 1,
            [
                'imgUrl' => '/img/bg-showcase-2.jpg',
                'headerValue' => 'Updated For Bootstrap 4',
                'pValue' => 'Newly improved, and full of great utility classes, Bootstrap 4
                            is leading the way in mobile responsive web development! All of
                            the themes on Start Bootstrap are now using Bootstrap 4!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
//            [ 'id_id' => 2,
            [
                'imgUrl' => '/img/bg-showcase-3.jpg',
                'headerValue' => 'Easy to Use &amp; Customize',
                'pValue' => 'Landing Page is just HTML and CSS with a splash of SCSS for
                            users who demand some deeper customization options. Out of
                             the box, just add your content and images, and your new
                             landing page will be ready to go!',
                'lang_id' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

        ]);

    }
}
