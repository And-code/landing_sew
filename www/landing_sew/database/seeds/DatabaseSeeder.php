<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
         $this->call(LangSeeder::class);
         $this->call(IconsGridSeeder::class);
         $this->call(ImageShowcaseSeeder::class);
         $this->call(TestimonialSeeder::class);
         $this->call(VariousDataSeeder::class);
         $this->call(VisibilitySeeder::class);
    }
}
