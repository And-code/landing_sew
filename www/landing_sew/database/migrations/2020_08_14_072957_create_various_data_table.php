<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariousDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('various_data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('properties');
            $table->unsignedBigInteger('lang_id')->nullable(false);

            $table->foreign('lang_id')
                ->references('id')->on('langs')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('various_data');
    }
}
