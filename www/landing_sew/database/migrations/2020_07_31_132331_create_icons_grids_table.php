<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIconsGridsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icons_grids', function (Blueprint $table) {
            $table->id();
//            $table->integer('id_id')->unique()->autoIncrement();
            $table->text('iconName');
            $table->text('headerValue');
            $table->text('pValue');
            $table->unsignedBigInteger('lang_id')->nullable(false);

            $table->foreign('lang_id')
                ->references('id')->on('langs')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icons_grids');
    }
}
